FROM alpine:3.12
RUN apk add --no-cache alpine-sdk git libbsd-dev libarchive-dev zlib-dev bzip2-dev xz-dev
RUN git clone --depth 1 https://github.com/freebsd/pkg /usr/src/pkg
WORKDIR /usr/src/pkg
RUN ./configure
RUN make
RUN make install
